# PIZZA 42 APP
* [BACKEND](https://tom-pizza42-backend.herokuapp.com)
* [FRONTEND](https://tom-pizza42-frontend.herokuapp.com)

## General overview

This repository contains 3 folders:

* backend: server side code
* frontend: SPA
* rules: JS scripts to add in the Auth0 rules configuration section 

### Frontend

This application has been built using Angular 5. Products come from the backend 
globally the cart is not persisted yet as it no extra value in the scope of this 
technical exercise. Authentication is done using Lock and authentication and sign up 
methods are Facebook and standard email/password.

In order to run this application locally, simply run the following commands:
```
cd frontend
yarn install
ng serve --aot
```
By default, a development server will run on http://localhost:4200. Backend is already 
configured to be at the following URL http://localhost:8080 once launched (see below)

Configuration is made using standard environment-{env}.ts file in `src/environments`

### Backend
The backend has been built using Spring Boot 2. To run this spring boot application, you 
will need to have java and maven installed on your local machine. If I had more time, and if you
want to, I can also prepare a `docker-compose.yml` with a ready to local run configuration (but this is nice to have).   
In order to run the backend, simply execute the following commands:
```
cd backend
mvn spring-boot:run
```
The backend exposes 2 routes:

* `/products`: to get the pizzas
* `/order`: to make an order

Configuration is made using standard application-{profile}.yml file in `src/main/resources`

*Security considerations: in the scope of this POC, the AuthenticationService.java class is used
as a method argument resolver to inject an Auth0 UserInfo object based on the received access_token into the order controller
method. Please use a real security framework in the real world* 

### Rules
There are 3 rules used by the Auth0 webtasks engine.

* `getFriendsCount.js`: gets the friends count of a user provided by FB
* `getGender.js`: uses firstName and lastName stored in the user_metadata object to fetch 
the https://genderize.io/ API. If you want to use this feature for auth0 provided users, feel free
to add `firstName` and `lastName` to the `additionalSignUpFields`option in Lock
* `linkAccounts.js`: links 2 accounts (eg. Auth0 and FB) with the same email address by keeping 
the verified one as `originalUser` and merges both `user_metadata` in the `originalUser`

**FB Apps Disclaimer: Since the Cambridge Analytica scandal, it seems the some sensitive scopes
such as user_friends have to pass strict validation checks before publishing an app. Application is submitted
to FB administration. If not plublic, don't hesitate to provide users to who I can grant developer access to the FB App** 