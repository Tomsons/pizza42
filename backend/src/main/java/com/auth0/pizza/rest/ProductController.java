package com.auth0.pizza.rest;

import com.auth0.pizza.domain.Pizza;
import com.auth0.pizza.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ProductController {

    @Autowired
    private ProductService productService;

    @GetMapping("/products")
    public Pizza[] products() {
        return productService.getAllProducts();
    }
}
