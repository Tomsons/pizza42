package com.auth0.pizza.service;

import com.auth0.pizza.domain.Pizza;
import org.springframework.stereotype.Service;

@Service
public class ProductService {
    public Pizza[] getAllProducts() {
        return new Pizza[] {
            new Pizza(1, "Pizza 1", "Description pizza 1", 14),
            new Pizza(2, "Pizza 2", "Description pizza 2", 22),
            new Pizza(3, "Pizza 3", "Description pizza 3", 10),
            new Pizza(4, "Pizza 4", "Description pizza 4", 23),
            new Pizza(5, "Pizza 5", "Description pizza 5", 15),
            new Pizza(6, "Pizza 6", "Description pizza 6", 13.5),
            new Pizza(7, "Pizza 7", "Description pizza 7", 9),
            new Pizza(8, "Pizza 8", "Description pizza 8", 17),
            new Pizza(9, "Pizza 9", "Description pizza 9", 16),
            new Pizza(10, "Pizza 10", "Description pizza 10", 11)
        };
    }
}
