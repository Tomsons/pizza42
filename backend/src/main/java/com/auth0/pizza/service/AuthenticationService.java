package com.auth0.pizza.service;

import com.auth0.exception.Auth0Exception;
import com.auth0.json.auth.UserInfo;
import com.auth0.pizza.config.Auth0Config;
import com.auth0.pizza.exception.UnauthorizedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.MethodParameter;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

@Service
public class AuthenticationService implements HandlerMethodArgumentResolver {

    @Autowired
    private Auth0Config auth0Config;

    @Override
    public boolean supportsParameter(MethodParameter methodParameter) {
        return methodParameter.getParameterType().equals(UserInfo.class);
    }

    @Override
    public Object resolveArgument(MethodParameter methodParameter, ModelAndViewContainer modelAndViewContainer, NativeWebRequest nativeWebRequest, WebDataBinderFactory webDataBinderFactory) throws Exception {
        String accessToken = getAccessToken(nativeWebRequest.getHeader("Authorization"));
        try {
            UserInfo userInfo = auth0Config.getClient().userInfo(accessToken).execute();
            Boolean emailVerified = (Boolean) userInfo.getValues().get("email_verified");
            if (!emailVerified) {
                throw new UnauthorizedException("Email has to be verified");
            }
            return userInfo;
        } catch (Auth0Exception e){
            throw new UnauthorizedException(e);
        }
    }

    private String getAccessToken(String authorization) {
        if (authorization == null || !authorization.startsWith("Bearer ")) {
            throw new UnauthorizedException("Missing valid authorization header");
        }
        return authorization.split(" ")[1];
    }
}
