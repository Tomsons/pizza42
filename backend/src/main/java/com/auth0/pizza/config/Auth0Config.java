package com.auth0.pizza.config;

import com.auth0.client.auth.AuthAPI;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@ConfigurationProperties(prefix = "auth0")
@Configuration
public class Auth0Config {

    private String domain;
    private String clientId;
    private String clientSecret;
    private AuthAPI client;

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getClientSecret() {
        return clientSecret;
    }

    public void setClientSecret(String clientSecret) {
        this.clientSecret = clientSecret;
    }

    public AuthAPI getClient() {
        if (client == null) {
            client = new AuthAPI(domain, clientId, clientSecret);
        }
        return client;
    }
}
