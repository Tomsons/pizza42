function fbFriendsCount(user, context, callback) {
  const FB = require('fbgraph@1.3.0');
  const _ = require('lodash@4.8.2');

  // Checks if the user has a facebook identity
  function getFacebook() {
    return user.identities.filter(function(identity) {
      return identity.provider === 'facebook' && identity.isSocial;
    });
  }

  // Updates the friends_count metadata property for the given user
  function updateFriendsCount(count) {
    const metadata = _.get(user, 'user_metadata') || {};
    metadata.friends_count = count;
    metadata.firstName = user.given_name;
    metadata.lastName = user.family_name;
    user.user_metadata = metadata;

    auth0.users.updateUserMetadata(user.user_id, metadata)
      .then(() => {
        callback(null, user, context);
      }, (error) => {
        callback(error);
      });
  }

  if (!user.email || getFacebook().length === 0 ) {
    return callback(null, user, context);
  }

  var fbIdentity = getFacebook()[0];
  // Sets the access token
  FB.setAccessToken(fbIdentity.access_token);

  // Gets the friends count
  FB.get("me", { fields: 'friends' },  function(err, res) {
    if (err) {
      return callback(err);
    }
    updateFriendsCount(res.friends.summary.total_count);
  });
}
