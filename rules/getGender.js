function getGender (user, context, callback) {

  const request = require('request@2.56.0');
  const _ = require('lodash@4.8.2');
  const Promise = require('promise@7.0.1');

  const metadata = _.get(user, 'user_metadata') || {};

  const getGender = (firstName) => new Promise((resolve, reject) => {
    request(`https://api.genderize.io/?name=${firstName}`, (error, response, body) => {
      if (error) {
        reject(error);
      } else if (response.statusCode !== 200) {
        reject(new Error(body));
      } else {
        const data = JSON.parse(body);
        resolve(data.gender);
      }
    });
  });

  if (metadata.firstName && !metadata.gender) {
    getGender(metadata.firstName)
      .then(gender => {
        metadata.gender = gender;
        return metadata;
      })
      .then(metadata => auth0.users.updateUserMetadata(user.user_id, metadata))
      .then(callback(null, user, context));
  } else {
    callback(null, user, context);
  }
}
