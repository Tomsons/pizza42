function linkAccounts(user, context, callback) {

  const request = require('request@2.56.0');
  const _ = require('lodash@4.8.2');
  const Promise = require('promise@7.0.1');

  /**
   * Loads all the users with the given email address as a Promise
   */
  const loadUsersForEmail = (email) => new Promise((resolve, reject) => {
    request({
      url:  `${auth0.baseUrl}/users-by-email`,
      headers: {
        Authorization: `Bearer ${auth0.accessToken}`
      },
      qs: {
        email: email
      }
    }, (err, response, body) => {
      if (response.statusCode === 200) {
        resolve(JSON.parse(body));
      } else {
        reject(new Error(body));
      }
    });
  });

  /**
   * Append identity to the given userId
   */
  const updateIdentities = (userId, providerInfo) => new Promise((resolve, reject) => {
    request.post({
      url:  `${auth0.baseUrl}/users/${userId}/identities`,
      headers: {
        Authorization: `Bearer ${auth0.accessToken}`
      },
      json: {provider: providerInfo.provider, user_id: providerInfo.user_id}
    }, (err, response) => {
      if (response && response.statusCode >= 400) {
        reject(new Error(`Error linking account: ${response.statusMessage}`));
      } else {
        resolve();
      }
    });
  });

  /**
   * Merge users by keeping the verified one as reference
   */
  const mergeUsersAndCallback = (user, otherUser) => {
    console.log(`Linking ${user.user_id} and ${otherUser.user_id} for email ${user.email}`);
    let originalUser = otherUser;
    let providerInfo = user.identities[0];

    // Keep the valid one to link with
    if (user.email_verified) {
      originalUser = user;
      providerInfo = otherUser.identities[0];
    }

    const metadata = _.merge(user.user_metadata || {}, otherUser.user_metadata || {});
    console.log(metadata);

    auth0.users.updateUserMetadata(originalUser.user_id, metadata)
      .then(updateIdentities(originalUser.user_id, providerInfo))
      .then(() => {
        context.primaryUser = originalUser.user_id;
        callback(null, user, context);
      })
      .catch(err => callback(err));
  };

  /**
   * Starts here
   */
  loadUsersForEmail(user.email)
    .then(users => users.filter(u => u.user_id !== user.user_id))
    .then(users => {
      if (users.length > 1) {
        throw new Error(`[!] Rule: Multiple user profiles already exist with email ${user.email} - cannot select base profile to link with`);
      }
      return _.first(users);
    })
    .then(otherUser => {
      if (!otherUser) {
        console.log(`Could not find other account with email ${user.email}. Skipping ...`);
        callback(null, user, context);
      } else {
        mergeUsersAndCallback(user, otherUser);
      }
    })
    .catch(err => callback(err));
}