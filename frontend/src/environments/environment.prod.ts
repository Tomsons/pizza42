export const environment = {
  production: true,
  backendUrl: 'https://tom-pizza42-backend.herokuapp.com',
  auth0Config: {
    clientID: 'ATyVN5EzgGqylXZguw2Dk6Uj7st139mR',
    domain: 'tomsons-fr.eu.auth0.com',
    responseType: 'token id_token',
    audience: 'https://tomsons-fr.eu.auth0.com/userinfo',
    redirectUri: 'https://tom-pizza42-frontend.herokuapp.com/callback',
    scope: 'openid profile email'
  }
};
