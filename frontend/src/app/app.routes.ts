import { Route } from '@angular/router';
import { MainComponent } from './layout/main/main.component';
import { ProductService } from './services/product.service';
import { ProductComponent } from './pages/product/product.component';
import { HomeComponent } from './pages/home/home.component';
import { AuthenticationService } from './services/authentication.service';
import { CallbackComponent } from './pages/callback/callback.component';
import { OrderComponent } from './pages/order/order.component';
import { CartService } from './services/cart.service';

export const ROUTES: Route[] = [
  {
    path: '',
    pathMatch: 'prefix',
    component: MainComponent,
    resolve: {
      pizzas: ProductService,
      user: AuthenticationService
    },
    children: [
      {
        path: '',
        component: HomeComponent
      },
      {
        path: 'products',
        component: ProductComponent,
        resolve: {
          pizzas: ProductService
        }
      },
      {
        path: 'order',
        component: OrderComponent,
        resolve: {
          cart: CartService,
          user: AuthenticationService
        }
      }
    ]
  },
  {
    path: 'callback',
    component: CallbackComponent
  }
];
