import { Component, Input, OnInit } from '@angular/core';
import { AuthenticationService } from '../../services/authentication.service';
import { User } from '../../domain/user';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  @Input('user')
  user: User;
  navBarOpen = false;

  constructor(private auth: AuthenticationService) { }

  ngOnInit() {}

  performLogin() {
    this.auth.login();
  }

  performLogout() {
   this.auth.logout();
  }
  
  toggleNavBar() {
    this.navBarOpen = !this.navBarOpen;
  }

}
