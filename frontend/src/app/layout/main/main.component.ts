import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { User } from '../../domain/user';
import { AuthenticationService } from '../../services/authentication.service';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {

  user: User;

  constructor(private route: ActivatedRoute, private auth: AuthenticationService) { }

  ngOnInit() {
    this.route.data.subscribe(({user}) => this.user = user);
    this.auth.userEmitter.subscribe(user => this.user = user);
  }

}
