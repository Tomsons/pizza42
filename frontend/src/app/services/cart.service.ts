import { EventEmitter, Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Pizza } from '../domain/pizza';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';

@Injectable()
export class CartService implements Resolve<Pizza[]> {
  
  cart: Pizza[] = [];
  cartEmitter = new EventEmitter<Pizza[]>();

  constructor(private http: HttpClient) {
    this.cart = JSON.parse(localStorage.getItem('cart') || '[]');
  }
  
  addToCart(pizza) {
    this.cart.push(pizza);
    localStorage.setItem('cart', JSON.stringify(this.cart));
    this.cartEmitter.emit(this.cart);
  }
  
  placeOrder() {
    return this.http.post(`${environment.backendUrl}/order`, {});
  }
  
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Pizza[]> | Promise<Pizza[]> | Pizza[] {
    return this.cart;
  }
  
  removeOne(id: number) {
    const otherPizzas = this.cart.filter(p => p.id !== id);
    const thesePizzas = this.cart.filter(p => p.id === id);
    thesePizzas.pop();
    this.cart = otherPizzas.concat(thesePizzas);
    localStorage.setItem('cart', JSON.stringify(this.cart));
    this.cartEmitter.emit(this.cart);
    return Observable.of(this.cart);
  }
  
  getTotal() {
    return this.cart.reduce((v, pizza) => {
      v += pizza.price;
      return v;
    }, 0);
  }
  
  clear() {
    this.cart = [];
    localStorage.removeItem('cart');
    this.cartEmitter.emit(this.cart);
  }
}
