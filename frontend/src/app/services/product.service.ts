import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Pizza } from '../domain/pizza';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable()
export class ProductService implements Resolve<Pizza[]> {

  private pizzas: Pizza[];
  
  constructor(private http: HttpClient) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Pizza[]> | Promise<Pizza[]> | Pizza[] {
    return this.http.get<Pizza[]>(`${environment.backendUrl}/products`);
  }
}
