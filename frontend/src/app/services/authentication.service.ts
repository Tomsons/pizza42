import { EventEmitter, Injectable } from '@angular/core';
import * as Auth0 from 'auth0-js';
import { environment } from '../../environments/environment';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/first';
import { User } from '../domain/user';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { CartService } from './cart.service';

@Injectable()
export class AuthenticationService implements Resolve<User>, HttpInterceptor {

  private auth;
  public userEmitter = new EventEmitter<User>();

  constructor(private cart: CartService) {
    this.auth = new Auth0.WebAuth(environment.auth0Config);
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<User> | Promise<User> | User {
    return this.isAuthenticated() ? this.loadUserProfile() : undefined;
  }

  login() {
    this.auth.authorize();
  }

  parseHashResult() {
    return Observable.create((obs) => {
      this.auth.parseHash((err, result) => {
        if (err) {
          obs.error(err);
        } else {
          obs.next(this.setSession(result));
        }
        return obs;
      });
    });
  }

  private setSession(authResult): void {
    const expiresAt = JSON.stringify((authResult.expiresIn * 1000) + new Date().getTime());
    localStorage.setItem('access_token', authResult.accessToken);
    localStorage.setItem('id_token', authResult.idToken);
    localStorage.setItem('expires_at', expiresAt);
    return authResult;
  }

  public isAuthenticated(): boolean {
    const expiresAt = JSON.parse(localStorage.getItem('expires_at'));
    return new Date().getTime() < expiresAt;
  }

  private loadUserProfile() {
    return Observable.create((obs) => {
      const accessToken = localStorage.getItem('access_token');
      this.auth.client.userInfo(accessToken, (err, profile) => {
        if (err) {
          this.userEmitter.emit(undefined);
          obs.next(undefined);
        } else {
          this.userEmitter.emit(profile as User);
          obs.next(profile as User);
        }
      });
      return obs;
    }).first();
  }

  public logout() {
    localStorage.removeItem('access_token');
    localStorage.removeItem('id_token');
    localStorage.removeItem('expires_at');
    this.userEmitter.emit(undefined);
    this.cart.clear();
  }
  
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (this.isAuthenticated()) {
      request = request.clone({
        setHeaders: {
          Authorization: `Bearer ${localStorage.getItem('access_token')}`
        }
      });
    }
    return next.handle(request);
  }
}
