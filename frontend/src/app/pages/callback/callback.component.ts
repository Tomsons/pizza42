import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '../../services/authentication.service';
import 'rxjs/operator/do';

@Component({
  selector: 'app-callback',
  templateUrl: './callback.component.html',
  styleUrls: ['./callback.component.scss']
})
export class CallbackComponent implements OnInit {

  loading: boolean;

  constructor(private router: Router, private auth: AuthenticationService) {
  }

  ngOnInit() {
    this.loading = true;
    this.auth.parseHashResult()
      .subscribe(
        () => {
          this.loading = false;
          this.router.navigate(['']);
        },
        (err) => {
          this.loading = false;
          console.log(err);
          this.router.navigate(['']);
        }
      );
  }

}
