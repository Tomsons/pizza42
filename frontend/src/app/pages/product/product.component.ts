import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Pizza } from '../../domain/pizza';
import { CartService } from '../../services/cart.service';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit {

  pizzas: Pizza[];

  constructor(private route: ActivatedRoute, private cartService: CartService) { }

  ngOnInit() {
    this.route.data.subscribe(({ pizzas }) => this.pizzas = pizzas as Pizza[]);
  }

  addToCart(pizza: Pizza) {
    this.cartService.addToCart(pizza);
  }
  
  removeFromCart(pizza: Pizza) {
    this.cartService.removeOne(pizza.id)
      .subscribe();
  }
  
  getCount(pizza: Pizza) {
    return this.cartService.cart.filter(p => p.id === pizza.id).length;
  }
}
