import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Pizza } from '../../domain/pizza';
import { CartService } from '../../services/cart.service';
import { User } from '../../domain/user';
import { AuthenticationService } from '../../services/authentication.service';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.scss']
})
export class OrderComponent implements OnInit {
  
  cart: Pizza[];
  user: User;

  constructor(private route: ActivatedRoute, private cartService: CartService, private auth: AuthenticationService) { }

  ngOnInit() {
    this.route.data.subscribe(({cart, user}) => {
      this.cart = cart;
      this.user = user;
    });
    this.auth.userEmitter.subscribe(user => this.user = user);
    this.cartService.cartEmitter.subscribe(cart => this.cart = cart);
  }
  
  placeOrder() {
    this.cartService.placeOrder()
      .subscribe(console.log, console.error);
  }
  
  performLogin() {
    this.auth.login();
  }
  
  performLogout() {
    this.auth.logout();
  }
  
  getTotal() {
    return this.cartService.getTotal();
  }
}
